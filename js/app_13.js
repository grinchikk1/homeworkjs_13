"use strict";

const img = document.querySelector('.image-to-show');
const stopButton = document.querySelector('#stop');
const resumeButton = document.querySelector('#resume');
const images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg'];
const intervalTime = 3000;
let currentImageIndex = 0;
let timerId = null;

showImage();

startBtn();



function showImage() {
  currentImageIndex = (currentImageIndex + 1) % images.length;
  img.src = `img/${images[currentImageIndex]}`;
  img.style.opacity = 1;
  updateTimer(intervalTime);
}

function hideImage() {
  img.style.opacity = 0;
}

function startBtn() {
    timerId = setInterval(() => {
      hideImage();
      setTimeout(showImage, 500);
    }, intervalTime);
  }

function stopBtn() {
  clearInterval(timerId);
  timerId = null;
}

function updateTimer(time) {
  let remainingTime = time / 1000;
  const timerElement = document.querySelector('.timer');
  timerElement.textContent = remainingTime.toFixed(2);
  const countdownId = setInterval(() => {
    remainingTime -= 0.01;
    timerElement.textContent = remainingTime.toFixed(2);
    if (remainingTime <= 0) {
      clearInterval(countdownId);
    }
  }, 10);
}

stopButton.addEventListener('click', stopBtn);
resumeButton.addEventListener('click', startBtn);
